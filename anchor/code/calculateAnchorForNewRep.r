##########################################################
#
#
# aktana-learning Anchor estimates Aktana Learning Engines.
#
# description: predict anchor for rep does not have enough interaction records 
#
#
# created by : shirley.xuf@aktana.com
#
# created on : 2018-11-20
# updated on : 2018-11-20
#
# Copyright AKTANA (c) 2018.
#
#
####################################################################################################################

library(data.table)
library(foreach)
library(doMC)
library(doRNG)
library(geosphere)
library(futile.logger)
library(bizdays)

########################
# func: calculateAnchorForNewRep.addCalendarAdherence
########################
calculateAnchorForNewRep.addCalendarAdherence <- function(calendarAdherenceOn, calendarLocs, repCalendarAdherence, repAccountCalendarAdherence, calAdconfThreshold, repTeamRep, logPrefix=""){
  
  # add default calendar adherence prob
  DEFAULT_COMPLETE_PROB <- 1
  calendarLocs[,`:=`(facilityProb=DEFAULT_COMPLETE_PROB, clusterProb=DEFAULT_COMPLETE_PROB)]
  if (!calendarAdherenceOn) {
    return(calendarLocs)
  }
  
  # get repId from calendarLocs
  repid <- calendarLocs$repId[1]
  
  # check if repCalendarAdherence
  if (nrow(repCalendarAdherence)<=0) {
    flog.info("repCalendarAdherence is empty, cannot get calcualted rep calendar adherence, use default")
    return(calendarLocs)
  }
  
  # subset repCalendarAdherence & repAccountCalendarAdherence use repid
  repCalAdherence <- repCalendarAdherence[repId==repid]
  repAccountCalAdherence <- repAccountCalendarAdherence[repId==repid]
  if (nrow(repCalAdherence)<=0) {
    flog.debug("%s: no calculated calendar adherence for this rep, use from other rep", logPrefix)
    # check if has same team rep
    temprepCalAdherence <- repCalendarAdherence
    sameTeamReps <- unique(repTeamRep[repTeamId %in% repTeamRep[repId==repid,]$repTeamId,]$repId)  # reps belongs to the same team as the new rep
    if (length(sameTeamReps)>0) {
      temprepCalAdherence <- repCalendarAdherence[repId %in% sameTeamReps,]
      if (nrow(temprepCalAdherence)==0) {
        flog.debug("%s: cannot find reps belong to the same team as the new rep already have calendar adherence, use average of all reps to compute calendar adherence", logPrefix)
        temprepCalAdherence <- repCalendarAdherence
      }
    } else {flog.debug("%s: cannot find reps belong to the same team, use average of all reps to compute calendar adherence", logPrefix)}
    # calculate calendar adherence as average
    avgCalAd <- mean(temprepCalAdherence$completeProb)
    calendarLocs[,`:=`(facilityProb=avgCalAd, clusterProb=avgCalAd)]
    return(calendarLocs)
  }
  
  # get rep-level calendar adherence & its confidence
  repCalAd <- repCalAdherence$completeProb
  repCalAdConf <- repCalAdherence$completeConfidence
  # check if rep-level calendar adherence above threhold, if not set to default value
  if (repCalAdConf<calAdconfThreshold) {repCalAd <- 1}
  
  # check rep+account level calendar adherence first
  if (nrow(repAccountCalendarAdherence)>0) {
    calendarLocs <- merge(calendarLocs, repAccountCalAdherence[,c("repId","accountId","completeProb","completeConfidence")], by=c("repId","accountId"), all.x=TRUE)
    # set those confidence level lowerthan threhold back to NA
    calendarLocs[completeConfidence<calAdconfThreshold, `:=`(completeProb=NA, completeConfidence=NA)]
    # fill NA ones with rep level calendar adhernce
    calendarLocs[is.na(completeConfidence), `:=`(completeProb=repCalAd, completeConfidence=repCalAdConf)]
  } else {
    # directly add rep level calendar adhernce
    calendarLocs[,`:=`(completeProb=repCalAd, completeConfidence=repCalAdConf)]
  }
  
  # add calendar ahereence as clusterProb & facilityProb & cleanup
  calendarLocs[, facilityProb:=completeProb]
  calendarLocs[, c("completeProb", "completeConfidence"):=NULL]
  calendarLocs[, clusterProb:=mean(facilityProb)]
  
  flog.debug("%s: finish add calendar adherence", logPrefix)
  
  return(calendarLocs)
}

########################
# main fucntion
########################
calculateAnchorForNewRep <- function(newRepList, future, repAccountAssignments, repTeamRep, accountDateLocation, result_cluster, numberCores, predictRundate, predictAheadDayListFull, calendarAdherenceOn, repCalendarAdherence, repAccountCalendarAdherence, calAdprobThreshold, calAdconfThreshold, fixOutput) {
  
  flog.info("Start calculateAnchorForNewRep: for %s reps", nrow(newRepList))
  # ensure fixed output for automation testing
  if (fixOutput) setorder(accountDateLocation, accountId, dayOfWeek, facilityId)

  # get best day of week to visit an account
  flog.info("get the best day of week to visit the account")
  if (nrow(accountDateLocation)>0) {
    randomMaxFromGroup <- function(dayOfWeek,facilityId,latitude,longitude,facilityDoWScore) {
      ind <- which(facilityDoWScore==max(facilityDoWScore))
      randomIndexInGroup <- ifelse(length(ind)>1, sample(ind, 1), ind)
      return (list(dayOfWeek=dayOfWeek[randomIndexInGroup], facilityId=facilityId[randomIndexInGroup], latitude=latitude[randomIndexInGroup], longitude=longitude[randomIndexInGroup], state=NA, facilityProb=facilityDoWScore[randomIndexInGroup]))
    }
    bestDoWForAccount <- accountDateLocation[, randomMaxFromGroup(dayOfWeek,facilityId,latitude,longitude,facilityDoWScore), by="accountId"]
  } else {
    flog.info("accountDateLocation is empty, cannot calculateAnchorForNewRep")
    return(data.table())
  }
  
  # summarise maxNear
  flog.info("summarize maxNear for each predicted rep")
  if (nrow(result_cluster)>0) {
    maxNearFarRep <- unique(result_cluster[,c("repId","maxNearDistance","maxFarDistance")], by="repId")
  } else {
    flog.info("result from calculateAnchor is empty, cannot infer maxNearDistance and maxFarDistance for new rep")
    maxNearFarRep <- data.table()
  }
  
  # set up for parrallel
  numberCores <- min(numberCores, nrow(newRepList))
  registerDoMC(numberCores)  # setup the number of cores to use
  N <- data.table(reps=newRepList$repId,group=1:numberCores)
  print("print processed new rep list for each core:")
  print(paste(as.list(N), collapse=','))
  flog.info('start parallel processing')
  
  RESULT <- foreach(nc = 1:numberCores , .combine=rbind) %dorng%                   # loop for each core
  {
    # allocate the result table
    result <- list()
    resultPointer <<- 1
    
    # start loop
    reps <- N[group==nc]$reps
    
    for (repP in reps) {
      
      flog.debug("Core %s: Start processing for repId=%s",nc,repP)
      
      # take source from newRepList for logging
      source <- newRepList[repId==repP]$source
      logPrefix <- sprintf("Core %s: repId=%s (%s)", nc, repP, source)
      # take predictAheadDayList for days needed
      predictAheadDayList <- newRepList[repId==repP]$predictAheadDayList
      if (is.na(predictAheadDayList)) {
        predictAheadDayList <- predictAheadDayListFull
      } else {
        predictAheadDayList <- as.Date(strsplit(predictAheadDayList, split=',')[[1]])
      }
      
      
      # get maxNear & maxFar
      if (nrow(maxNearFarRep)>0) {
        tempmaxNearFarRep <- maxNearFarRep
        sameTeamReps <- unique(repTeamRep[repTeamId %in% repTeamRep[repId==repP,]$repTeamId,]$repId)  # reps belongs to the same team as the new rep
        if (length(sameTeamReps)>0) {
          tempmaxNearFarRep <- maxNearFarRep[repId %in% sameTeamReps,]
          if (nrow(tempmaxNearFarRep)==0) {
            flog.debug("%s: cannot find reps belong to the same team as the new rep already have prediction, use average of all reps to compute maxFar & maxNear", logPrefix)
            tempmaxNearFarRep <- maxNearFarRep
          }
        } else {flog.debug("%s: cannot find reps belong to the same team, use average of all reps to compute maxFar & maxNear", logPrefix)}
        temp <- tempmaxNearFarRep[,.(maxNearDistance=mean(maxNearDistance,na.rm=TRUE), maxFarDistance=mean(maxFarDistance,na.rm=TRUE))]
        maxNearDistance <- temp$maxNearDistance
        maxFarDistance <- temp$maxFarDistance
      } else {
        maxNearDistance <- NA
        maxFarDistance <- NA
      }
      flog.debug("%s maxNearDistance=%s, maxFarDistnace=%s", logPrefix, maxNearDistance, maxFarDistance)
      
      # check whether has future planned visits, and write to results if any
      calendarLocs <- future[repId==repP & date %in% predictAheadDayList]
      if(dim(calendarLocs)[1]>0) {# if there are planned visits for this rep in a future day
        # compose calendar planned visits output
        calendarLocs <- calendarLocs[,c("repId","date","accountId","facilityId","latitude","longitude")]
        calendarLocs$state <- NA
        calendarLocs$source <- 'calendar'
        calendarLocs$maxNearDistance <- maxNearDistance
        calendarLocs$maxFarDistance <- maxFarDistance
        flog.debug("%s has %s planned visit in the predict ahead time frame", logPrefix, dim(calendarLocs)[1])
        # add calendar adherence prob
        calendarLocs <- calculateAnchorForNewRep.addCalendarAdherence(calendarAdherenceOn, calendarLocs, repCalendarAdherence, repAccountCalendarAdherence, calAdconfThreshold, repTeamRep, logPrefix)
        
        # add to result list
        calendarLocs <- calendarLocs[,c("repId","date","clusterProb","maxNearDistance","maxFarDistance","accountId","facilityId","latitude","longitude","state","facilityProb","source")]
        result[[resultPointer]] <- calendarLocs; resultPointer <<- resultPointer+1
      }
      
      # get what's the best DoW to visit the account assigned to rep
      flog.debug("%s: finish processing to get best DoW to visit the account assigned to rep", logPrefix)
      bestDoWForVisit <- bestDoWForAccount[accountId %in% repAccountAssignments[repId==repP,]$accountId, ]
      
      # predict using accountDateLocation
      flog.debug("%s: start looping to predict", logPrefix)
      predictedStates <- rep(NA,length(predictAheadDayList))
      names(predictedStates) <- as.character(predictAheadDayList)
      # for each day in the future where predictions are required (offLoopSize days)
      for(predictDate in as.list(predictAheadDayList)) # need to loop over list otherwise will not give data object
      {
        # get predict day of week
        predDOW <- weekdays(predictDate)
        
        # get which accounts are best to visit on this day
        temp_bestDoWForVisit <- bestDoWForVisit[dayOfWeek==predDOW,-c("dayOfWeek")]
        if (nrow(temp_bestDoWForVisit)>0) {
          predictedStates[[as.character(predictDate)]] <- "territory(all)"
          
          # check whether has calender event
          ff <- calendarLocs[date==predictDate]
          flog.debug("%s predictDay:%s has %s calendar events before check calAdprobThreshold", logPrefix, as.character(predictDate), nrow(ff))
          # if calendarAdherenceOn further check if prob greater than threshold
          if (nrow(ff)>0 & calendarAdherenceOn) {
            ff <- ff[facilityProb>=calAdprobThreshold]
            flog.debug("%s predictDay:%s has %s calendar events after check calAdprobThreshold", logPrefix, as.character(predictDate), nrow(ff))
          }
          if(dim(ff)[1]>0) {# if there are planned visits for this rep in a future day
            flog.debug("%s predictDay:%s use calendar events to further filter suggested visit accountList", logPrefix, as.character(predictDate))
            predictedStates[[as.character(predictDate)]] <- "territory(filter by calendar)"
            
            # maxNear is needed for filtering
            if (!is.na(maxNearDistance)) {
              # calcualte planned visit center (as weighted center by facility prob)
              planLocs <- ff[,.(longitude=sum(longitude*facilityProb, na.rm=TRUE)/sum(facilityProb, na.rm=TRUE), latitude=sum(latitude*facilityProb, na.rm=TRUE)/sum(facilityProb, na.rm=TRUE))]
              locs <- rbind(planLocs,temp_bestDoWForVisit[,c("longitude","latitude")])
              # calculate distance between suggest visit and planned visit
              dd <- distm(locs[,list(longitude,latitude)])
              # pick the ones that within maxNear to planned visit
              temp_bestDoWForVisit <- temp_bestDoWForVisit[which(dd[1,2:ncol(dd)]<=maxNearDistance),][!accountId %in% ff$accountId]
            } else {
              flog.debug("%s predictDay:%s no maxNearDistance available, cannot use calendar events to further filter suggested visit accountList", logPrefix, as.character(predictDate))
              predictedStates[[as.character(predictDate)]] <- "territory(all, not filter by calendar as no maxNear)"
            }
          }
          
          # check again whether it still has suggested account after filtering by calendar
          if (nrow(temp_bestDoWForVisit)>0) {
            result[[resultPointer]] <- data.table(repId=repP, date=predictDate, clusterProb=mean(temp_bestDoWForVisit$facilityProb), maxNearDistance=maxNearDistance, maxFarDistance=maxFarDistance, temp_bestDoWForVisit[,c("accountId","facilityId","latitude","longitude","state","facilityProb")], source=source)
            resultPointer <<- resultPointer+1
          } else {
            flog.debug("%s: predictDay:%s no assigned accounts are best to visit after filtering using calendar event", logPrefix, as.character(predictDate))
            predictedStates[[as.character(predictDate)]] <- "NA(no assigned accounts are best to visit after filtering by calendar event)"
          }
          
        } else {
          flog.debug("%s: predictDay:%s no assigned accounts are best to visit ", logPrefix, as.character(predictDate))
          predictedStates[[as.character(predictDate)]] <- "NA(no assigned accounts are best to visit)"
        }
      }
      flog.info("%s finish prediction, maxNearDistance=%s, maxFarDistance=%s, predict result (%s Days) :%s", logPrefix,maxNearDistance, maxFarDistance, length(predictedStates), paste(predictedStates,collapse="->"))
      
    }
    
    if(resultPointer>1) result <- rbindlist(result[1:(resultPointer-1)]) else result <- data.table()
    flog.info("Core %s: finish processing, dim(result)=(%s)", nc, paste(dim(result),collapse=","))
    return(result)
  }
  
  flog.info("Return from calculateAnchorForNewRep: add newRepResult(dim=(%s)) to anchorResult", paste(dim(RESULT),collapse=","))
  return(RESULT)
}