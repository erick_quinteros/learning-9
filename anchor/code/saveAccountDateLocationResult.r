##########################################################
#
#
# aktana-learning Anchor estimates Aktana Learning Engines.
#
# description: save anchor results
#
#
# created by : jacob.adicoff@aktana.com
# updated by : shirley.xu@aktana.com
#
# created on : 2018-11-26
# updated on : 2018-12-07
#
# Copyright AKTANA (c) 2015.
#
#
####################################################################################################################
library(futile.logger)
library(Learning.DataAccessLayer)


saveAccountDateLocationScores <- function(result, RUN_UID, BUILD_UID, predictRundate) {
  
  flog.info("Enter saveAccountDateLocationScores Result")
  # add learningBuildUID
  result$learningRunUID <- RUN_UID
  result$learningBuildUID <- BUILD_UID
  result$runDate <- predictRundate
  # delete old data first
  dataAccessLayer.anchor.truncate.AccountDateLocationScores()
  # write result back to DB
  flog.info("save %s row of AccountDateLocationScores to Learning", dim(result)[1])
  dataAccessLayer.anchor.write.AccountDateLocationScores_l(result)
}

saveAccountDateLocation <- function(result, RUN_UID, BUILD_UID, isNightly) {
  flog.info("Enter saveAccountDateLocation Result")
  
  # add leanringUID
  result$learningRunUID <- RUN_UID
  
  if(isNightly) {
    # delete column not needed
    result[,c("facilityDoWScore","runDate"):=NULL]
    
    # delete old data
    dataAccessLayer.anchor.truncate.AccountDateLocation()
    # write result back to DB
    # write back to table (overwrite)
    flog.info("save %s row of AccountDateLocation to DSE", dim(result)[1])
    dataAccessLayer.anchor.write.AccountDateLocation_dse(result)
  } else {
    result$learningBuildUID <- BUILD_UID
    # delete old data
    dataAccessLayer.anchor.deleteFrom.AccountDateLocation(BUILD_UID,RUN_UID) # remove old build records in Learning DB
    # write result back to DB
    # write back to table (overwrite)
    flog.info("save %s row of AccountDateLocation to Learning", dim(result)[1])
    dataAccessLayer.anchor.write.AccountDateLocation_l(result)
  }
}
