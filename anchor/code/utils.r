##########################################################
#
#
# aktana- utils
#
# description: script to hold the helper function to anchor module
#
# created by : shirley.xu@aktana.com
#
# updated on: 2018-07-31
#
# Copyright AKTANA (c) 2018.
#
#
####################################################################################################################
library(bizdays)
library(futile.logger)


############################################
# Name:  utils.generatePredictAheadDayList
# Desc:  generate a vector of dates given predictAhead Date (skip weekends)
# Param:  predictRundate, predictAhead
# Return: a vector of (predictAhead+1) weekday dates starting predictRundate if predictRundate is weekday
#         or a vector of (predictAhead) weekday dates starting predictRundate if predictRundate is weekend
############################################
utils.generatePredictAheadDayList <- function(predictRundate, predictAhead) {
  cal <- create.calendar(name="USA/ANBIMA", weekdays=c("saturday", "sunday"))
  predictAheadDayList <- bizseq(predictRundate, offset(predictRundate,predictAhead,cal), cal)
  return (predictAheadDayList)
}


############################################
# Name:  utils.determineIfRunWeeklyRecalculation
# Desc:  determine if a recalcualtion is needed for weekly calcuation table (e.g. accountDateLocationScores,
#         RepCalendarAdherence, RepAccountCalendarAdherence)
# Param:  predictRundate, isNightly, resultSaveTableData, BUILD_UID
# Return: a logical variable whether recalcualtion is needed
############################################

utils.determineIfRunWeeklyRecalculation <- function(predictRundate, isNightly, resultSaveTableData, BUILD_UID) {
  # check if weekly run
  isWeekly <- F
  if(isNightly && weekdays(predictRundate) == "Sunday") isWeekly <- T
  
  # determine whether needs to run accountDateLocationScores calcualtions or not
  emptyData <- T
  newBuild <- F
  needsRefresh <- F
  if (nrow(resultSaveTableData) > 0) {
    emptyData <- F
    if(BUILD_UID != resultSaveTableData$learningBuildUID[1]) newBuild <- T
    if(abs(as.Date(resultSaveTableData$runDate[1])-predictRundate)>7) needsRefresh <- T
  }
  recalculate <- (isWeekly || !isNightly || emptyData || newBuild || needsRefresh)
  if (recalculate) {
    flog.info("Recalculate as isWeekly=%s, isNightly=%s, empty_adlScores=%s, newBuild=%s, needsRefresh=%s", isWeekly, isNightly, emptyData, newBuild, needsRefresh)
  } else {
    flog.info("Not recalculate isWeekly=%s, isNightly=%s, empty_adlScores=%s, newBuild=%s, needsRefresh=%s, using from learningRunUID=%s", isWeekly, isNightly, emptyData, newBuild, needsRefresh, resultSaveTableData$learningRunUID[1])
  }
  
  return(recalculate)
}

#
## plotting function if you want to visualize the cluster estimates (repAvgDayLoc)
#
plotData <- function(title,x,Hfit)
{
  library(lattice)
  print(xyplot(aveLat~aveLong,group=state,x,type=c('p'),cex=1,main=title,pch=20))
  #        if(class(Hfit)=="hclust")print(plot(Hfit,main=title))
  print(xyplot(aveLat~aveLong,x,type=c('p','l'),cex=.8,main=title,pch=20))
  x[,c("lat","long"):=list(mean(aveLat),mean(aveLong)),by=state]
  #        print(xyplot(lat~long,x,type=c('p','l'),cex=1,main=title,pch=20))
  x[,c("from","to"):=list(state,state)]
  x$to <- x[2:(dim(x)[1]-1)]$to
  x$ctr <- 1
  x[,thickness:=sum(ctr),by="from"]
  x[,label:=max(thickness),by="from"]
  p <- xyplot(lat~long,x,pch=20,type=c('p','l'),cex=log(x$label),main=title)
  print(p)
  trellis.focus("panel",1,1)
  panel.text(x=p$panel.args[[1]]$x,y=p$panel.args[[1]]$y,labels=x$label,pos=3)
  #        trellis.unfocus()
}