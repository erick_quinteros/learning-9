context('testing calculateCalendarAdherence script in ANCHOR module')
print(Sys.time())

library(futile.logger)
flog.threshold(DEBUG)

# load library and source script
source(sprintf("%s/anchor/code/calculateCalendarAdherence.r",homedir))

# test calculateCalendarAdherence# load required input data
load(sprintf('%s/anchor/tests/data/from_loadAnchorData_callHistory.RData', homedir))
tmp <- calculateCalendarAdherence(callHistory)
repCalendarAdherence_new <- tmp[["repCalendarAdherence"]]
repAccountCalendarAdherence_new <- tmp[["repAccountCalendarAdherence"]]
rm(tmp)

# test cases
test_that("test calculated repCalendarAdherence, repCalendarAdherence same as saved", {
  # test dimension
  expect_equal(dim(repCalendarAdherence_new),c(2,5))
  expect_equal(dim(repAccountCalendarAdherence_new),c(8,6))
  # test same as saved
  load(sprintf('%s/anchor/tests/data/from_calculateCalendarAdherence_repCalendarAdherence.RData', homedir))
  load(sprintf('%s/anchor/tests/data/from_calculateCalendarAdherence_repAccountCalendarAdherence.RData', homedir))
  expect_equal(repCalendarAdherence_new,repCalendarAdherence)
  expect_equal(repAccountCalendarAdherence_new,repAccountCalendarAdherence)
})