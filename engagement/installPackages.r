##########################################################
#
#
# aktana-learning Install Aktana Learning Engines.
#
# description: install needed packages for engagement
#
#
# created by : marc.cohen@aktana.com
#
# created on : 2016-11-29
#
# Copyright AKTANA (c) 2016.
#
#
####################################################################################################################
#
# install Packages needed for Engagement code
# build function library used by the Engagement driver code
#
##########################################################
args <- commandArgs(TRUE)

##args is now a list of character vectors
## First check to see if arguments are passed.
## Then cycle through each element of the list and evaluate the expressions.
if(length(args)==0){
    print("No arguments supplied.")
    quit(save = "no", status = 1, runLast = FALSE)
}else{
    print("Arguments supplied.")
    for(i in 1:length(args)){
      eval(parse(text=args[[i]]))
      print(args[[i]]);
    }
}

# install packages needed by the Engagement functions

needed <- c("RMySQL","data.table","ks","foreach","doMC","mvtnorm","rgl","uuid","properties","reshape2","testthat")
options(downLoad.file.method="curl")
inst <- installed.packages()
for(i in needed)
  {
      if(!is.element(i,inst))
          {
              install.packages(i,repos="https://cran.rstudio.com/")
          }
  }

#
# This code reads in each of the .r files that defines a function
# and then saves them to the EngagementLibrary
#

source(sprintf("%s/engagement/code/loadEngagementData.R",homedir))
source(sprintf("%s/engagement/code/calculateEngagement.R",homedir))
source(sprintf("%s/engagement/code/calculateTriggerEngagement.R",homedir))
source(sprintf("%s/engagement/code/saveEngagementResult.R",homedir))

save(loadEngagementData,
     calculateEngagement,
     calculateTriggerEngagement,
     E,
     saveEngagementResult,
     file=sprintf("%s/EngagementLibrary.RData",libdir))
