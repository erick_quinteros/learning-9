##########################################################
#
#
# aktana- engagement estimates estimates Aktana Learning Engines.
#
# description: read the data
#
# created by : marc.cohen@aktana.com
#
# created on : 2016-11-01
#
# Copyright AKTANA (c) 2016.
#
#
####################################################################################################################

loadEngagementData <- function(con, schema, startHorizon, useForProbability)
{
    library(data.table)
    library(futile.logger)
        
    dbGetQuery(con,"SET NAMES utf8")

    # read the interactions table
    flog.info("read Interaction table")
    interactions <- data.table(dbGetQuery(con,sprintf("select * from %s.Interaction where startDateLocal>'%s';",schema,startHorizon))) 
    interactions[,eventdate:=as.Date(startDateLocal)]

    # read the interaction account table
    flog.info("read InteractionAccount table")
    interactionAccount <- data.table(dbGetQuery(con,sprintf("select interactionId, accountId from %s.InteractionAccount;",schema)))
    interactions <- merge(interactions,interactionAccount,by="interactionId") # merge with the interactions

    # read the account table
    flog.info("read Account table")
    accounts <- data.table(dbGetQuery(con,sprintf("select accountId, externalId from %s.Account;",schema)))
    setnames(accounts,"externalId","accountUID")

    # read the interaction product table
    #interactionProduct <- data.table(dbGetQuery(con,sprintf("select interactionId, productId from %s.InteractionProduct;",schema)))

    # read the rep table
    flog.info("read Rep table")
    reps <- data.table(dbGetQuery(con,sprintf("select repId, externalId from %s.Rep;",schema)))
    setnames(reps,"externalId","repUID")

    flog.info("read repAssignments table")
    repAssignments <- data.table(dbGetQuery(con,"SELECT repId, accountId FROM RepAccountAssignment;"))

    # from RepAccountAssignment, get only accounts existing in Rep table
    accts <- unique(repAssignments[repId %in% reps$repId]$accountId)
    interactions <- interactions[accountId %in% accts]    # filtering interactions using accountId

    # update accounts and reps using RepAccountAssignment
    accounts <- accounts[accountId %in% accts]

    repIds <- unique(repAssignments[repId %in% reps$repId]$repId)
    reps <- reps[repId %in% repIds]

    # flatten the interactions with merges of accounts and reps
    interactions <- merge(interactions,accounts,by="accountId")
    interactions <- merge(interactions,reps,by="repId")
    interactions <- interactions[,c("accountUID","accountId","repUID","repId","eventdate","repActionTypeId"),with=F]
    interactions$reaction <- "Touchpoint"   # prepare for merge with the suggestions tables
    setnames(interactions,"eventdate","date")

    # read the feedback table from the stage db
    flog.info("read stage db Suggestion_Feedback_vod__c_arc table")
    table <- sprintf("%s_stage.Suggestion_Feedback_vod__c_arc",schema)
    feedback <- unique(data.table(dbGetQuery(con,sprintf("select RecordTypeId, LastModifiedDate, Suggestion_vod__c from %s;",table))))
    setnames(feedback,"LastModifiedDate","feedbackDate")

    # read the record types from the stage db
    flog.info("read stage db RecordType_vod__c_arc table")
    table <- sprintf("%s_stage.RecordType_vod__c_arc",schema)
    recordType <- data.table(dbGetQuery(con,sprintf("select Id, Name from %s;",table)))
    setnames(recordType,"Id","RecordTypeId")
    feedback <- merge(feedback,unique(recordType),by="RecordTypeId")    # merge the recordtype into the feedback table to flatten
    setnames(feedback,c("Name"),c("reaction"))
    feedback[reaction=="Activity_Execution_vod",reaction:="Execution"]  # rename feedback keys
    feedback[reaction=="Dismiss_vod",reaction:="Dismiss"]
    feedback[reaction=="Mark_As_Complete_vod",reaction:="Complete"]

    interactions[,c("accountUID","repUID"):=NULL]  # more cleanup of uneeded fields

    # read the suggestion table from the stage db
    flog.info("read stage db Suggestion_vod__c_arc table")

    table <- sprintf("%s_stage.Suggestion_vod__c_arc",schema)
    suggestions <- data.table(dbGetQuery(con,sprintf("select Id, OwnerId, Account_vod__c, Record_Type_Name_vod__c, Suggestion_External_Id_vod__c, CreatedDate, Title_vod__c from %s where Call_Objective_Record_Type_vod__c='Suggestion_vod' and Call_Objective_From_Date_vod__c>'%s';",table,startHorizon)))
    setnames(suggestions,c("Id","OwnerId","Account_vod__c","Record_Type_Name_vod__c","CreatedDate"),c("Suggestion_vod__c","repUID","accountUID","type","date"))
    suggestions[type=="Call_vod",repActionTypeId:=3]                                                                  ############### Kludge - this is to match the keys used in the DSE DB
    suggestions[type=="Email_vod",repActionTypeId:=12]                                                                ############### Kludge - this is to match the keys used in the DSE DB
    suggestions[type=="Call_vod" & regexpr("~r[[:digit:]]+~W~",Suggestion_External_Id_vod__c)>1,repActionTypeId:=13]  ############### Kludge - this is to match the keys used in the DSE DB
    suggestions[,date:=as.Date(date)]

    # merge the suggestions and associated feedback if any
    flog.info("merge suggestions and feedback")
    suggestions <- merge(suggestions,feedback,by="Suggestion_vod__c",all.x=T)
    suggestions[is.na(reaction),reaction:="Ignored"]

    # cleanup uneeded fields
    suggestions[,c("Suggestion_vod__c","RecordTypeId","Name"):=NULL]

    # flatten the suggestions table
    suggestions <- merge(suggestions,accounts,by="accountUID")
    suggestions <- merge(suggestions,reps,by="repUID")

    # more cleanup of uneeded fields
    suggestions[,c("accountUID","repUID"):=NULL]

    flog.info("Done with loadEngagementData.")
    # get global addressability for these tables
    interactions <<- interactions
    suggestions <<- suggestions
}
