
##########################################################
##
##
## aktana-learning Install Aktana Learning Engines.
##
## description: Driver Code
## 1. analyze message opens/click likelihoods
##
## created by : marc.cohen@aktana.com
##
## created on : 2015-11-03
##
## Copyright AKTANA (c) 2015.
##
##
##########################################################
library(h2o)
library(data.table)
library(Hmisc)
library(futile.logger)
library(sparklyr)
library(dplyr)

###########################
## Func: messageSequence.prepareDynamicDesignMatrix
###########################
messageSequence.prepareDynamicDesignMatrix <- function(interactions, interactionsP, targetNames, DAYS_CONSIDER_FOR_PRIORVISIT) {
  
  # subset the interactions (which include events concatenated in the load data) for the target messages
  ints <- interactions %>% filter(physicalMessageUID %in% targetNames & productInteractionTypeName %in% c("RTE_CLICK","RTE_OPEN","SEND") & !is.na(physicalMessageUID)) # only include sends, opens, and clicks
  # rm(interactions,envir=parent.frame())                                                          # clean up memory
  
  # prepare priorVisit features
  intsV <- interactionsP %>% filter(productInteractionTypeName=="VISIT_DETAIL") %>%
    select(accountId,date)       # get Visit data from
  # rm(interactionsP,envir=parent.frame())                                                        # clean up memory
  # gc()

  # add index to ints for tracking which has already has priorVisit added
  ints <- sdf_with_unique_id(ints, id="id")
  # subset ints may have prior visit
  intsHasV <- ints %>% filter(productInteractionTypeName=="SEND") %>%
    left_join(intsV, by="accountId", suffix = c("_s", "_v")) %>%                 # join visit records
    filter(date_s >= date_v) %>%                                                 # filter visit happens before send
    group_by(id) %>%                                                             # group by per send record
    summarise(priorVisit=as.numeric(datediff(MAX(date_s),MAX(date_v))))        # find out the date betweens last visit and send
  # append results back to interactions
  ints <- ints %>% left_join(intsHasV,by="id") %>%                              
    mutate(priorVisit=ifelse(is.na(priorVisit),-1,priorVisit)) %>%               # fill NA
    select(-id) %>%                                                              # delete id column
    # create keys based on the event and the message identifier
    mutate(key = ifelse(productInteractionTypeName=="RTE_OPEN", paste0("OPEN___",physicalMessageUID),
                        ifelse(productInteractionTypeName=="RTE_CLICK", paste0("CLIC___",physicalMessageUID),
                               paste0("SEND___",physicalMessageUID)))) %>%       
    mutate(actionOrder = ifelse(productInteractionTypeName=="RTE_OPEN", as.numeric(2),
                                ifelse(productInteractionTypeName=="RTE_CLICK", as.numeric(3),
                                       as.numeric(1))))
  return(ints)
}


###########################
## Main function
###########################

messageSequence <- function(config, modelSaveDir, interactions, accountProduct, products, emailTopicNames, interactionsP, targetNames=NULL)
{
    ## constant settings
    options("h2o.use.data.table" = TRUE)    # speed up h2o object conversion
    DAYS_CONSIDER_FOR_PRIORVISIT <- 30      # constant config for priorVisit

################################### read parameters from the configuration file ##############################
    flog.info("Reading configuration")
    pName <- products$productName
    includeVisitChannel <- getConfigurationValueNew(config,"LE_MS_includeVisitChannel")
    if (is.null(includeVisitChannel)) {includeVisitChannel <- 1}
    removeMessageSends <- getConfigurationValueNew(config,"LE_MS_removeMessageSends")
    removeMessageClicks <- getConfigurationValueNew(config,"LE_MS_removeMessageClicks")
    removeMessageOpens <- getConfigurationValueNew(config,"LE_MS_removeMessageOpens")
    RFtreeDepth <- getConfigurationValueNew(config,"LE_MS_RFtreeDepth")
    RFtreeNo <- getConfigurationValueNew(config,"LE_MS_RFtreeNo")
    messageAnalysisUseML <- getConfigurationValueNew(config,"LE_MS_messageAnalysisUseML")
    if(messageAnalysisUseML==0) messageAnalysisUseML <- "RF"
    prods <- getConfigurationValueNew(config,"LE_MS_addPredictorsFromAccountProduct")
    modelDesc <- getConfigurationValueNew(config,"modelType")
    modelName <- getConfigurationValueNew(config,"buildUID")
    targetType <- substr(getConfigurationValueNew(config,"LE_MS_messageAnalysisTargetType"), 1, 4) # only first 4 characters of type
    messageAnalysisAllMessages <- 0
    if(length(targetNames)==0){ flog.warn("Modeling all messages."); messageAnalysisAllMessages <- 1;}

#################################### build static design matrix ############################################
    flog.info("Processing static predictors")
    temp <- sparkBuildStaticDesignMatrix(prods, accountProduct, emailTopicNames, logDropProcess=TRUE)
    AP <- temp[["AP"]] %>% collect() %>% data.table()
    APpredictors <- temp[["APpredictors"]]
    predictorNamesAPColMap <- temp[['predictorNamesAPColMap']]
    rm(temp)
    tbl_uncache(spark_connection(accountProduct), as.character(accountProduct$ops$x[1]))
    # db_drop_table(spark_connection(accountProduct), "accountProduct")

##################### get message lists that will be looped to build model individually #####################
    if(messageAnalysisAllMessages==1) {targetNames <- interactions %>% filter(!is.na(physicalMessageUID)) %>% pull(physicalMessageUID) %>% unique()} # gather potential targets from the interactions table
    #    targetNames <- tolower(targetNames[targetNames!=".na"])

########################## build dynamic design matrix (part common to all messages) #########################
    flog.info("Processing dynamic predictors")
    ints <- messageSequence.prepareDynamicDesignMatrix(interactions, interactionsP, targetNames, DAYS_CONSIDER_FOR_PRIORVISIT) %>% collect() %>% data.table()
    tbl_uncache(spark_connection(interactions), as.character(interactions$ops$x)[1])
    tbl_uncache(spark_connection(interactionsP), as.character(interactionsP$ops$x)[1])
    closeSpark(spark_connection(interactions))
    
############################ start to build models to messages individually ####################################

    targetNames <- paste0(targetType,"___",targetNames)                                          # append the open or click target type to the message identifer
    flog.info("Number of targets: %s",length(targetNames))
    
    flog.info("Analyzing product: %s",pName)
    output <- data.table(names=character())                                                      # set up to save the results of the modeling
    models <- data.table(target=character(),AUC=numeric(),Total=numeric(),Accuracy=numeric(),Precision=numeric(),Misclassification=numeric(),TPR=numeric(),FPR=numeric(),Specificity=numeric(),Prevalence=numeric(),modelName=character())

    flog.info("Starting target loop")
################# loop through each target
    for(targetName in targetNames)
    {
      flog.info("Analyze %s",targetName)
      sendName <- gsub(targetType,"SEND",targetName)                                           # setup for finding sends of the target message
      
      flog.info("number of TargetName records: %s",dim(ints[key==targetName])[1])
      flog.info("number of SendName records: %s",dim(ints[key==sendName])[1])
      if(dim(ints[key==targetName])[1]==0) {
        flog.info("Finish modeling, Nothing to model as there are no targets in the data.")
        next                             # nothing to model if there are no targets in the data
      }
      if(dim(ints[key==sendName])[1]==0) {
        flog.info("Finish modeling, Nothing to model as there are no sends of the target message in the data.")
        next                             # nothing to model if there are no sends of the target message in the data
      }
      ################ build dynmaic design matrix unique to each message
      flog.info("build dynamic features in design matrix (account's previous open/send/click msg behavior)")
      t <- messageDesign(ints,sendName,targetName)                                             # build dynamic components for the design - this function is in the learning package
      
      # further process priorVisits (encode to binary)
      encodePriorVisitMultiBinary <- function(priorVisitC) {
        priorVisitList <- rep(list(0), DAYS_CONSIDER_FOR_PRIORVISIT+1)
        if (priorVisitC >= 0 & priorVisitC <= DAYS_CONSIDER_FOR_PRIORVISIT) {
          priorVisitList[[priorVisitC+1]] <- 1
        }
        return (priorVisitList)
      }
      flog.info("processing priorVisit feature from Visit Channel, make it into binary")
      t[,paste("priorVisit_", 0:DAYS_CONSIDER_FOR_PRIORVISIT, "_days_before_send" ,sep=""):=encodePriorVisitMultiBinary(priorVisit),by=1:nrow(t)]
      t$priorVisit <- NULL
      
      flog.info("merging dynamic and static features to get the full design matrix: dim(dynamic_feature) = (%s)", paste(dim(t),collapse=","))
      allModel <- merge(t,AP,by="accountId",all=T)                                             # merge dynamic design components to static design
      allModel$accountId <- NULL                                                               # don't need the accountId in the design matrix
      
      ################ final processing of design matrix (check dim, unqiue value, etc)
      flog.info("check for number of design matrix records > 20? dimension of design matrix = (%s)",paste(dim(allModel),collapse=","))
      if(dim(allModel)[1]>20)                                                                  # if there are fewer than 21 observations than skip building a model
      {
        flog.info("check again to make sure targetName is one of columns in design matrix")
        if(!(targetName %in% names(allModel))) {
          flog.info("Finish modeling, Nothing to model as there are no targets in design matrix.")
          next                                           # if the target not in model than skip build - not sure if this is needed since looks like checked above??
        }
        
        allModel[is.na(allModel)] <- 0
        postiveTargetInAllModel <- sum(allModel[,targetName,with=F])
        flog.info("more than 5 positive target records needed to continue - found %s",postiveTargetInAllModel)
        if(postiveTargetInAllModel>5)                                                        # if there are fewer than 6 observations with the target than skip building a model
        {
          allModel <- eval(parse(text=sprintf("allModel[%s>0]",sendName)))                 # only include rows that have a send of the target message in the design
          flog.info("dimension of design matrix with positive send records: (%s)",paste(dim(allModel),collapse=','))
          
          if(dim(unique(allModel[,targetName,with=F]))[1]<2)                               # if there all target observations are either only opens (clicks) or not-opens (not-clicks) than flip one record
          {                                                                                # to artificially create a contrast
            flog.info("Only 1 type of target value = %s",allModel[1,targetName,with=F])  # this will at least enable some more model based data collection for future rebuild
            samp <- sample( 1:(dim(allModel)[1]), 1 )
            eval(parse(text=sprintf("allModel[samp,%s:=abs(%s-1)]",targetName,targetName)))
          }
          
          colsInAllModel <- names(allModel)                                                # for later log dropped predictor
          allModel <- allModel[,sapply(allModel,function(x)length(unique(x))>1),with=F]    # pick out variable withs more than 1 unique value
          droppedPredictors <- colsInAllModel[!colsInAllModel %in% names(allModel)]                                  # log dropped predictors
          flog.info("%s of %s predictors from design matrix allModel(static + dynamic features) dropped in target loop as they have less than 2 unique values after subsetting with send and filling NA with 0", length(droppedPredictors), length(colsInAllModel))
          flog.debug(paste(droppedPredictors, collapse=","))
          
          if (dim(allModel)[2] == 0) {
            flog.info("all predictors are dropped. skip this message")
            next
          }

          ############## final selection of features used to build model based on learning.properties config                
          # find the column numbers for setup to the h2o calls
          allCols <- 1:(dim(allModel)[2])
          visitCols <- grep("priorVisit", names(allModel))
          clickCols <- grep("CLIC",names(allModel))
          sendCols <- grep("SEND",names(allModel))
          openCols <- grep("OPEN",names(allModel))
          clickName <- gsub("OPEN.","CLIC",targetName)
          i <- which(names(allModel)==targetName)
          clickNameCols <- which(names(allModel)==clickName)
          sendNameCols <- which(names(allModel)==sendName)
          
          # next line needed for sends, opens, and account attributes to be included as predictors
          xvars <- allCols
          if(removeMessageSends==1)xvars <- xvars[!xvars %in% sendCols]                    # next line needed to remove send from predictors
          if(removeMessageClicks==1)xvars <- xvars[!(xvars %in% clickCols)]                # next line needed to remove clicks from predictors
          if(removeMessageOpens==1)xvars <- xvars[!(xvars %in% openCols)]                  # next line needed to remove opens from predictors
          if(includeVisitChannel==0)xvars <- xvars[!(xvars %in% visitCols)]                  # remove visit info if configs indicates not include
          if(length(clickNameCols)>0)xvars <- xvars[!(xvars %in% clickNameCols)]           # remove click of target open             
          if(length(sendNameCols)>0)xvars <- xvars[!(xvars %in% sendNameCols)]     # remove send of target open or click
          if (targetType=="OPEN") { xvars <- xvars[xvars!=gsub(targetType,"CLIC",targetName)]
          } else if (targetType=="CLIC") { xvars <- xvars[xvars!=gsub(targetType,"OPEN",targetName)] }  # drop click/open column of target message if target is open/click
          xvars <- unique(xvars)
          xVars <- xvars[xvars!=i]
          droppedPredictors <- names(allModel)[allCols[!(allCols %in% xvars)]]    # log dropped predictors
          flog.info("%s of %s remaining predictors in the design matrix dropped because of learning.properties config setting: %s",length(droppedPredictors),length(allCols),paste(droppedPredictors,collapse=","))
          flog.info("final design matrix dimension = (%s,%s)", dim(allModel)[1], length(xVars)+1)
          keptPredictors <- names(allModel)[allCols[(allCols %in% xVars)]]
          
          ################ start training
          # now start to convert the design to an h2o object ()
          # note that tmp.hex <- as.h2o(allModel) is slow for large object
          nrow_t <- dim(allModel)[1]
          fwrite(allModel, "/tmp/allModel.csv", col.names=FALSE)                                            # data.table fwrite is fast
          
          # h2o.importFile is very fast due to parallel processing
          tmp.hex = h2o.importFile(path="/tmp/allModel.csv", destination_frame="tmp.hex", header=FALSE, col.names=names(allModel))
          flog.info("H2O object tmp.hex rows : %s", dim(tmp.hex)[1])
          # check for empty lines if insert by h2o, if any remove
          nrow_p <- dim(tmp.hex)[1]
          nrow_empty <- nrow_p - nrow_t
          if (nrow_empty > 0) {  # there are extra empty lines in the beginning of tmp.hex
            flog.info("slicing...")
            tmp.hex <- tmp.hex[(1+nrow_empty):nrow_p, ]  # remove empty lines in the beginning
          }
          flog.info("removed empty lines if any (insert by h2o). final tmp.hex rows = %s", dim(tmp.hex)[1])
          
          if(messageAnalysisUseML=="RF")                                                   # if the random forest is to be used
          {
            tmp.hex[,i] <- as.factor(tmp.hex[,i])                                        # make sure the target is a factor
            nFolds <- min(floor(dim(allModel)[1]/10),5)                                  # heuristic for cross-validation fold number
            if(nFolds==1)nFolds <- 0
            
            # Call Random Forest function to train model
            statusCode <- tryCatch(rf.hex <- h2o.randomForest(x=xVars,y=i,training_frame=tmp.hex,ntrees=RFtreeNo,max_depth=RFtreeDepth,nfolds=nFolds),
                                   error = function(e) {
                                     flog.info("Unable to build model for %s. Skip it.", targetName)
                                     return (NULL)
                                   }
            )
            if (is.null(statusCode)) {
              next  # unable to build model; skip it
            }
            
            print(sprintf("Message: %s",targetName))                                    
            print(rf.hex)
            
            o1 <- rf.hex@model$variable_importances                                      # capture the variable importance
            o1 <- data.table(o1$variable, o1$scaled_importance)
          } else if(messageAnalysisUseML=="GLM")                                             # if logistic regression is to be used
          {
            rf.hex <- h2o.glm(x=xVars,y=i,training_frame=tmp.hex,lambda=0)               # call the h2o.glm() function
            
            print(sprintf("Message: %s",targetName))
            print(rf.hex)
            
            o1 <- rf.hex@model$coefficients_table                                        # capture the coefficients
            o1 <- data.table(o1$names, o1$coefficients)
          } else { flog.error("Unknown ML Method") }
          
          ############### log training results (importance, accuracy, etc)
          setnames(o1, c("V1","V2"), c("names", targetName))                               # the following code is to calculate performance stats for reporting
          output <- merge(output,o1,by="names",all=T,fill=T)
          mName <- h2o.saveModel(object=rf.hex,path=modelSaveDir,force=T)
          t <- data.table(h2o.confusionMatrix(rf.hex))
          TN <- t[1]$'0'
          FP <- t[1]$'1'
          FN <- t[2]$'0'
          TP <- t[2]$'1'
          TOTAL <- t[3]$'0'+t[3]$'1'
          cm <- NULL
          cm[1] <- as.numeric(min(h2o.auc(rf.hex),h2o.auc(rf.hex,xval=T))) # use the minimum auc from training or cross-validation
          cm[2] <- TOTAL
          cm[3] <- (TP+TN)/TOTAL
          cm[4] <- TP/t[3]$'1'
          cm[5] <- (FP+FN)/TOTAL
          cm[6] <- TP/(FN + TP)
          cm[7] <- FP/(TN + FP)
          cm[8] <- TN/(TN + FP)
          cm[9] <- (FN + TP)/TOTAL
          names(cm) <- c("AUC","Total","Accuracy","Precision","Misclassification","TPR","FPR","Specificity","Prevalence")
          models <- rbind(models,cbind(data.table(t(cm)),data.table(target=targetName,modelName=gsub(getwd(),".",mName))))
          
          ########### clean up h2o objects
          h2o.removeAll()
          rm(tmp.hex)
          rm(rf.hex)
          gc()
          gc()
          flog.info("Finish modeling")
        } else { # sum(allModel[,targetName,with=F]) <= 5
          flog.info("Finish modeling, Nothing to model as there are not enough positive target records in design matrix to build model.") }
      } else { # dim(allModel)[1] <= 20
        flog.info("Finish modeling, Nothing to model as there are not enough records in design matrix to build model.") }
    }
    return (list(models=models, output=output, APpredictors=APpredictors, pName=pName, predictorNamesAPColMap=predictorNamesAPColMap))
}

