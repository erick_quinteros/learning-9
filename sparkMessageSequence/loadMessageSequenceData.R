#
#
# aktana-learning model building code for Aktana Learning Engines.
#
# description: This is initiallization code
#
#
# created by : marc.cohen@aktana.com
#
# created on : 2015-10-13
#
# Copyright AKTANA (c) 2015.
#
##########################################################
library(sparkLearning)
library(data.table)
library(properties)
library(futile.logger)
library(RMySQL)
library(sparklyr)
library(dplyr)

loadMessageSequenceData.expiredMessages <- function(con_cs, productUID) {
  flog.info("loading expired messages from _cs DB")
  expiredMessages <- dbGetQuery(con_cs,sprintf("SELECT Id FROM Approved_Document_vod__c WHERE Product_vod__c = '%s' AND Status_vod__c='Expired_vod';",productUID))
  expiredMessages <- expiredMessages[1:dim(expiredMessages)[1],1]
  print('Expired Messages:')
  print(expiredMessages)
  return(expiredMessages)
}

loadMessageSequenceData.messages <- function(messages) {
  flog.info("process messages from V3db")
  messages[,c("messageName"):=NULL]
  setnames(messages,"messageDescription","messageName")
  # messages$messageName <- iconv(messages$messageName, to='ASCII', sub="-") # handle special character like "–" etc
  return(messages)
}

loadMessageSequenceData.messageSetMessage <- function(messageSetMessage) {
  flog.info("process messageSetMessage from V3db")
  return(messageSetMessage)
}

loadMessageSequenceData.messageSet <- function(messageSet) {
  flog.info("process messageSet from V3db")
  return(messageSet)
}

loadMessageSequenceData.accountPredictorNames <- function(accounts) {
  flog.info("save accountPredictorNames from accounts from V3db")
  accountPredictorNames <- colnames(accounts)[!(colnames(accounts) %in% c("externalId","createdAt","updatedAt","facilityId","accountName","isDeleted","accountId"))]
  accountPredictorNames <- gsub("-","_",accountPredictorNames)
  return(accountPredictorNames)
}

loadMessageSequenceData.interactionsP <- function(sc, sparkDBconURL_l, interactions) {
  flog.info("process interactions from V3db to get interactionsP (filtered interactions")
  # get delivered status information from DB
  deliveredIntSQL <- "SELECT DISTINCT Id FROM Sent_Email_vod__c_arc WHERE Status_vod__c = 'Delivered_vod'"
  deliveredInt <- sparkReadFromDB(sc, sparkDBconURL_l, deliveredIntSQL, name="deliveredInt")
  # filter the message delivered
  flog.info('filtering internation data based on delivered_or_not information from Sent_Email_vod__c_arc')
  interactionsP <- interactions %>%
    filter(productInteractionTypeName!="VISIT_DETAIL") %>%
    inner_join(deliveredInt, by=c("externalId"="Id")) %>%
    sdf_bind_rows(filter(interactions, productInteractionTypeName=="VISIT_DETAIL")) %>%
    select(-externalId) %>%
    select(-c(facilityId,wasCreatedFromSuggestion, duration))      # cleaning
  # cacheing inteaction interactionsP
  flog.info("caching interactionsP")
  interactionsP <- sdf_register(interactionsP, "interactionsP_after_loadMessageSequenceData")
  tbl_cache(spark_connection(interactionsP), "interactionsP_after_loadMessageSequenceData")
  interactionsP <- tbl(spark_connection(interactionsP), "interactionsP_after_loadMessageSequenceData")
  return(interactionsP)
}

loadMessageSequenceData.messageTopic <- function(sc, sparkDBconURL_l, con_l, productUID) {
  flog.info("loading messageTopic (messageClusteringResult) from _l DB")
  messageTopicSQL <- sprintf("SELECT emailTopicId, emailTopicName, messageUID, documentDescription from AKT_Message_Topic_Email_Learned WHERE productUID='%s'",productUID)
  messageTopic <- sparkReadFromDB(sc, sparkDBconURL_l, messageTopicSQL, name="messageTopic", memory=TRUE)
  
  prodNo <- which(dbGetQuery(con_l,"SELECT DISTINCT productUID from AKT_Message_Topic_Email_Learned;")$productUID==productUID)  # this is needed to be compatible with old model
  messageTopic <- messageTopic %>% mutate(emailTopicId=as.integer(1000)*prodNo+emailTopicId) %>%
    dplyr::rename(physicalMessageUID=messageUID)
  # messageTopic$documentDescription <- iconv(messageTopic$documentDescription, to='ASCII', sub="-") # handle special character like "–" etc
  return(messageTopic)
}

loadMessageSequenceData.emailTopicNameMap <- function(messageTopic) {
  flog.info("compose emailTopicNameMap from loaded message clustering result (messageTopic)")
  emailTopicNameMap <- messageTopic %>% distinct(emailTopicId,emailTopicName) %>% collect() %>% data.frame()
  emailTopicNameMap <- setNames(as.list(emailTopicNameMap$emailTopicName), emailTopicNameMap$emailTopicId)
  return(emailTopicNameMap)
}

loadMessageSequenceData.events <- function(events) {
  flog.info("process events from V3db")
  # filter only open and click
  eventsP <- events %>% filter(eventTypeName %in% c("RTE_OPEN","RTE_CLICK"))
  # change to date data type
  eventsP <- eventsP %>% dplyr::rename(date=eventDate)  %>%
    select(-c(eventLabel,eventDateTimeUTC)) %>%    # cleaning
    dplyr::rename(productInteractionTypeName=eventTypeName)
  return(eventsP)
}

loadMessageSequenceData.interactions <- function(interactionsP, eventsP, messageTopic) {
  flog.info("get interactions from interactionsP, eventsP, messageTopic")
  # merge events
  interactions <- sdf_bind_rows(interactionsP,eventsP)
  # filter by only messages exists in messageTopic
  interactions <- interactions %>% filter(!is.na(physicalMessageUID)) %>%
    inner_join(select(messageTopic, physicalMessageUID), by="physicalMessageUID")
  flog.info("caching interactions")
  interactions <- sdf_register(interactions, "interactions_after_loadMessageSequenceData")
  tbl_cache(spark_connection(interactions), "interactions_after_loadMessageSequenceData")
  interactions <- tbl(spark_connection(interactions), "interactions_after_loadMessageSequenceData")
  return(interactions)
}

loadMessageSequenceData.emailTopicRates <- function(eventsP, messageTopic) {
  flog.info("estimate Message Topic propensities (get emailTopicRates)")
  # merge clustering results
  t <- eventsP %>% 
    select(accountId,productInteractionTypeName,physicalMessageUID) %>% 
    inner_join(select(messageTopic, c("physicalMessageUID","emailTopicId")), by="physicalMessageUID") %>% 
    select(accountId,productInteractionTypeName,emailTopicId) %>%
    mutate(ctr=as.numeric(1)) %>%
    group_by(accountId,productInteractionTypeName,emailTopicId) %>%
    mutate(count = sum(ctr, na.rm=TRUE)) %>%
    ungroup() %>%
    select(-ctr) %>%
    filter(!is.na(emailTopicId)) %>% 
    distinct()
  emailTopicRates <- t %>% mutate(temp = paste(productInteractionTypeName, as.integer(emailTopicId), sep="_")) %>%
    select(-productInteractionTypeName,-emailTopicId) %>%
    sdf_pivot(accountId~temp, fun.aggregate=list(count = "sum"))
  # tt <- t[,colSums(is.na(t))<nrow(t)]
  # emailTopicRates <- t[,names(tt)[tt],with=F]
  # names(emailTopicRates) <- gsub("-","_",names(emailTopicRates))  # remove -'s from names
  return(emailTopicRates)
}

loadMessageSequenceData.accountProduct <- function(accountProduct, accounts, emailTopicRates) {
  flog.info("process accountProduct and merge with accounts from V3db")
  accounts <- select(accounts, -c("externalId","createdAt","updatedAt","facilityId","accountName","isDeleted"))
  accountProduct <- select(accountProduct,-c("createdAt","updatedAt"))
  # spark dataframe manupilation
  accountProduct <- accountProduct %>% left_join(accounts, by="accountId", suffix = c("_x", "_y")) %>%
    left_join(emailTopicRates, by="accountId")
  
  flog.info("processing accountProduct colnames")
  # remove -'s from names
  originalColnames <- copy(colnames(accountProduct))
  newColnames <- gsub("-", "_", originalColnames)
  renameStr <- character()
  for (i in 1:length(originalColnames)) {
    newColname <- newColnames[i]
    oldColname <- originalColnames[i]
    if (newColname != oldColname) {
      renameStr <- c(renameStr, sprintf("%s=`%s`", newColname, oldColname, sep=','))
    }
  }
  if (length(renameStr)>0) {
    renameStr <- paste(renameStr, collapse=', ')
    eval(parse(text=sprintf("accountProduct <- rename(accountProduct, %s) ",renameStr)))
  }
  
  # find out charCols & remove \n\r as spark dataframe cannot have \n in column name
  colTypes <- sapply(sdf_schema(accountProduct),function(x){x$type})
  charCols <- names(colTypes)[colTypes=="StringType"]
  charCols <- charCols[charCols!="productName"]
  
  if (length(charCols)>0) {
    accountProduct <- accountProduct %>% mutate_at(.vars=charCols,funs(regexp_replace(.,'\n|\r','')))
    
    flog.info("Remove bad characters")
    # charColsAccountProduct <- accountProduct %>% select(one_of(charCols)) %>% spark_apply(function(df){data.frame(lapply(df, function(x) {return(gsub('[[:blank:]|,|>|<]','_',x))}))})
    # charColsAccountProduct <- accountProduct %>% select(one_of(charCols)) %>% mutate_all(funs(regexp_replace(.,'<|>|,| ','_')))
    # accountProduct <- accountProduct %>% select(-one_of(charCols)) %>% sdf_bind_cols(charColsAccountProduct)
    accountProduct <- accountProduct %>% mutate_at(.vars=charCols,funs(regexp_replace(.,'<|>|,|\\\\.| ','_')))
  }
  
  flog.info("caching accountProduct")
  accountProduct <- sdf_register(accountProduct, "accountProduct_after_loadMessageSequenceData")
  tbl_cache(spark_connection(accountProduct), "accountProduct_after_loadMessageSequenceData")
  accountProduct <- tbl(spark_connection(accountProduct), "accountProduct_after_loadMessageSequenceData")
  
  return(accountProduct)
}

loadMessageSequenceData.processMessageTopic <- function(messageTopic) {
  return(messageTopic %>% select(physicalMessageUID, documentDescription) %>% collect() %>% data.table())
}


##########################################################################################################
#                                               Main Function
###########################################################################################################

loadMessageSequenceData <- function(sc, sparkDBconURL, sparkDBconURL_l, con, con_l, con_cs, productUID, readDataList=NULL)
{
  loadedData <- list()
  # constant for data needed from readV3db
  DATA_NEEDED_FROM_READV3DB <- list(interactions=c("interactions","events"), accountProduct=c("accountProduct","accounts"), products=c("products"), messageSetMessage=c("messageSetMessage"), messageSet=c("messageSet"), emailTopicNames=NULL, messages=c("messages"), messageTopic=NULL, interactionsP=c("interactions","events"), expiredMessages=NULL, accountPredictorNames=c("accounts"), emailTopicNameMap=NULL)
  # compose readDataList
  if (is.null(readDataList)) {
    readDataList <- c("interactions","accountProduct","products","messageSetMessage","messageSet","emailTopicNames","messages","messageTopic","interactionsP","expiredMessages","accountPredictorNames","emailTopicNameMap")
    flog.info("loadMessageSequenceData: all (%s)", paste(readDataList,collapse=","))
  } else {
    flog.info("loadMessageSequenceData: %s", paste(readDataList,collapse=","))
  }
  
  # use readV3dbFilter to read data
  readV3dbDataList <- NULL
  for (outputData in readDataList) {
    readV3dbDataList <- c(readV3dbDataList, DATA_NEEDED_FROM_READV3DB[[outputData]])
    readV3dbDataList <- unique(readV3dbDataList)}
  dictraw <- sparkReadV3dbFilter(sc, sparkDBconURL, con, productUID, readDataList=readV3dbDataList) 
  for(i in 1:length(dictraw))assign(names(dictraw)[i],dictraw[[i]])
  rm(dictraw)
  
  # load data dependent on data from readV3db but independent of other loaded data
  if ("products" %in% readDataList) {loadedData[["products"]] <- products}
  if ("messageSetMessage" %in% readDataList) {loadedData[["messageSetMessage"]] <- loadMessageSequenceData.messageSetMessage(messageSetMessage)}
  if ("messageSet" %in% readDataList) {loadedData[["messageSet"]] <- loadMessageSequenceData.messageSet(messageSet)}
  if ("accountPredictorNames" %in% readDataList) {loadedData[["accountPredictorNames"]] <- loadMessageSequenceData.accountPredictorNames(accounts)}
  
  # load data that would use data from readV3db and use for processing other loaded data
  if (any(readDataList %in% c("interactions","accountProduct","emailTopicNames","messageTopic","emailTopicNameMap"))) {messageTopic <- loadMessageSequenceData.messageTopic(sc, sparkDBconURL_l, con_l, productUID)}
  if (any(readDataList %in% c("interactions","accountProduct","emailTopicNames"))) {eventsP <- loadMessageSequenceData.events(events)}
  if (any(readDataList %in% c("accountProduct","emailTopicNames"))) {emailTopicRates <- loadMessageSequenceData.emailTopicRates(eventsP, messageTopic)}
  if (any(readDataList %in% c("interactionsP","interactions"))) {interactionsP <- loadMessageSequenceData.interactionsP(sc, sparkDBconURL_l, interactions)}
  
  # load other data in the list of required dependent on each other
  if ("messages" %in% readDataList) {loadedData[["messages"]] <- loadMessageSequenceData.messages(messages)}
  if ("messageTopic" %in% readDataList) {loadedData[["messageTopic"]] <- loadMessageSequenceData.processMessageTopic(messageTopic)}
  if ("emailTopicNameMap" %in% readDataList) {loadedData[["emailTopicNameMap"]] <- loadMessageSequenceData.emailTopicNameMap(messageTopic)}
  if ("interactionsP" %in% readDataList) {loadedData[["interactionsP"]] <- interactionsP}
  if ("emailTopicNames" %in% readDataList) {loadedData[["emailTopicNames"]] <- colnames(emailTopicRates)[!colnames(emailTopicRates) %in% c("productName","accountId")]}
  if ("interactions" %in% readDataList) {loadedData[["interactions"]] <- loadMessageSequenceData.interactions(interactionsP, eventsP, messageTopic)}
  if ("accountProduct" %in% readDataList) {loadedData[["accountProduct"]] <- loadMessageSequenceData.accountProduct(accountProduct, accounts, emailTopicRates)}
  
  # load data independent of readV3db
  if ("expiredMessages" %in% readDataList) {loadedData[["expiredMessages"]] <- loadMessageSequenceData.expiredMessages(con_cs, productUID)}
  
  # final cleaning
  rm(accounts);rm(accountProduct);rm(interactions);rm(events)
  gc()

  flog.info("Return from loadMessageSequencedata")
  return(loadedData)
}
