##########################################################
#
#
# aktana-learning Install Aktana Learning Engines.
#
# description: install needed packages for anchor learning
#
#
# created by : marc.cohen@aktana.com
#
# created on : 2015-10-13
# modified on: 2015-10-27 - made multi-day and cleanup up
#
# Copyright AKTANA (c) 2015.
#
#
####################################################################################################################
#
# install Packages needed for Anchor code
# build function library used by the Anchor driver code
#
##########################################################
args <- commandArgs(TRUE)

##args is now a list of character vectors
## First check to see if arguments are passed.
## Then cycle through each element of the list and evaluate the expressions.
if(length(args)==0){
    print("No arguments supplied.")
    quit(save = "no", status = 1, runLast = FALSE)
}else{
    print("Arguments supplied.")
    for(i in 1:length(args)){
      eval(parse(text=args[[i]]))
      print(args[[i]]);
    }
}

# install packages needed by the Anchor functions

needed <- c("futile.logger","RMySQL","data.table","markovchain","Rsolnp","lattice","bizdays")
options(downLoad.file.method="curl")
inst <- installed.packages()
for(i in needed)
  {
      if(!is.element(i,inst))
          {
              install.packages(i,repos="https://cran.rstudio.com/")
          }
  }

#
# This code reads in each of the .r files that defines a function
# and then saves them to the AnchorlearningLibrary
#

source(sprintf("%s/anchor/code/loadAnchorData.r",homedir))
source(sprintf("%s/anchor/code/calculateAnchor.r",homedir))
source(sprintf("%s/anchor/code/saveAnchorResult.r",homedir))
plotData <- function(x,y){}  # remove debuggin plot from production version

anchorLibraryBuild <- list(buildDate=Sys.time(),functionsInLibrary=lsf.str())

save(anchorLibraryBuild,
     loadAnchorData,
     calculateAnchor,
     getLocation,
     plotData,
     getLocation,
     saveAnchorResult,
     file=sprintf("%s/AnchorLibrary.RData",libdir))
