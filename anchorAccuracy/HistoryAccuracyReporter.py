import pandas as pd
from datetime import datetime

from anchorAccuracy.AccuracyReportDataLoader import AccuracyReportDataLoaderDriver
from anchorAccuracy.AccuracyReportCalculator import AccuracyReportCalculator
from anchorAccuracy.AccuracyReportSaver import AccuracyReportSaver
from anchorAccuracy.utils.check_input import validate_date_format
from common.pyUtils.logger import get_module_logger

logger = get_module_logger(__name__)


class HistoryAccuracyReporter:
    """
    object to handle accuracy report for history run
    """

    def __init__(self, conn_pool, run_startDate, run_endDate):
        validate_date_format(run_startDate, "start date")
        validate_date_format(run_endDate, "end date")
        self.is_nightly = False
        self.run_startDate = run_startDate
        self.run_endDate = run_endDate
        self.conn_pool = conn_pool
        self.predict_df = None
        self.visit_df = None
        self.suggest_df = None
        self.predict_facility_df = None
        self.accuracy_result = None

    def load_data(self):
        data_loader = AccuracyReportDataLoaderDriver(self.conn_pool, is_nightly=self.is_nightly, start_date=self.run_startDate, end_date=self.run_endDate).run()
        self.predict_df = data_loader.predict_df
        self.visit_df = data_loader.visit_df
        self.suggest_df = data_loader.suggest_df
        self.predict_facility_df = data_loader.predict_facility_df
        self.run_startDate = data_loader.start_date
        self.run_endDate = data_loader.end_date

    def analyze_accuracy(self):
        self.accuracy_result = AccuracyReportCalculator(self.is_nightly, self.predict_df, self.visit_df, self.suggest_df, self.predict_facility_df).run().result

    def save_accuracy(self):
        AccuracyReportSaver(self.conn_pool, is_nightly=self.is_nightly, start_date=self.run_startDate, end_date=self.run_endDate, accuracy_result=self.accuracy_result).run()

    def run(self):
        logger.info("Start Running HistoryAccuracyReporter")
        self.load_data()
        self.analyze_accuracy()
        self.save_accuracy()
        logger.info("Finish Running HistoryAccuracyReporter for anchor history window {} to {}".format(self.run_startDate, self.run_endDate))
