CREATE TABLE `RepTeam` (
  `repTeamId` int(11) NOT NULL AUTO_INCREMENT,
  `repTeamName` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `externalId` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `seConfigId` int(11) DEFAULT NULL,
  `isDeleted` tinyint(4) NOT NULL,
  `createdAt` datetime DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`repTeamId`),
  UNIQUE KEY `repTeam_uniqueAK` (`externalId`),
  UNIQUE KEY `repTeam_uniqueTeamName` (`repTeamName`),
  KEY `seConfigId` (`seConfigId`)
) ENGINE=InnoDB AUTO_INCREMENT=1008 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci