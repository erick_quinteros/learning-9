##########################################################
#
#
# aktana- upload builds to s3
#
# description: upload builds to s3 after use on emr
#
# created by : learning-dev@aktana.com
#
# created on : 2019-06-05
#
# Copyright AKTANA (c) 2019.
#
#
####################################################################################################################
library(futile.logger)
library(jsonlite)

###########################
## utils funcs
###########################
# function to upload buildfolder to s3W
uploadBuildFolderToS3 <- function(localBuildFilesPath, s3BuildFilesPath) {
  # upload to s3
  shellCode <- sprintf("aws s3 cp %s %s --recursive --quiet", localBuildFilesPath, s3BuildFilesPath)
  system(shellCode)
}
# function to upload file to s3
uploadBuildFileToS3 <- function(localBuildFilesPath, s3BuildFilesPath, filePath) {
  fileLocalPath <- paste(localBuildFilesPath, filePath, sep='/')
  fileS3Path <- paste(s3BuildFilesPath, filePath, sep='/')
  # upload to s3
  shellCode <- sprintf("aws s3 cp %s %s", fileLocalPath, fileS3Path)
  system(shellCode)
}

############################################################
## Main script
############################################################
# set parameters 
args <- commandArgs(T)

if(length(args)==0){
  print("No arguments supplied.")
  if(batch)quit(save = "no", status = 1, runLast = FALSE)
} else {
  print("Arguments supplied.")
  for(i in 1:length(args)){
    eval(parse(text=args[[i]]))
    print(args[[i]]);
  }
}

# register error handler
cleanUp <- function () { 
  q('no',status=1) }
options(error=cleanUp)

# s3, build path common part
localBuildDirPath <- sprintf("%s/builds", normalizePath(homedir))
targetS3ConfigFilePath <- sprintf("%s/common/sparkUtils/s3Config.json", homedir)
if (file.exists(targetS3ConfigFilePath)) {
  s3BuildDirPath <- fromJSON(targetS3ConfigFilePath)[["s3BuildDirPath"]]
  flog.info("s3BuildDirPath is: %s", s3BuildDirPath)
} else {
  stop(sprintf("%s not exists, failed to get s3 builds path", targetS3ConfigFilePath))
}
# s3BuildDirPath <- sprintf("s3://aktana-bdpus-kafka/learningdata/%s/%s", customer, envname)

# check if nightly run
isNightly <- F
if(!exists("BUILD_UID")) {
  isNightly <- T
  BUILD_UID <- "all"
  localBuildFilesPath <- localBuildDirPath
  s3BuildFilesPath <- s3BuildDirPath
} else {
  localBuildFilesPath <- sprintf("%s/%s", localBuildDirPath, BUILD_UID)
  s3BuildFilesPath <- sprintf("%s/%s", s3BuildDirPath, BUILD_UID)
}

# read saved timestamp for when the copying of build finished
copyFinishTimeSavePath <- sprintf("%s/copyFinishTime.RData", logdir)

if (file.exists(copyFinishTimeSavePath)) {
  
  # load saved copy completion timestamp
  load(copyFinishTimeSavePath)
  flog.info("upload all files modified after %s to s3", as.character(copyFinishTime))
  
  # list files in target build folders 
  allFiles <- list.files(localBuildFilesPath, recursive=TRUE)
  filesMtime <- file.mtime(paste(localBuildFilesPath, allFiles, sep='/'))
  newFiles <- allFiles[filesMtime > copyFinishTime]
  
  # upload new files to s3
  for (buildFile in newFiles) {
    uploadBuildFileToS3(localBuildFilesPath, s3BuildFilesPath, buildFile)
  }
  
} else {
  
  flog.info("upload whole build folder <%s> to s3", BUILD_UID)
  # upload the whole builds
  uploadBuildFolderToS3(localBuildFilesPath, s3BuildFilesPath)
}
