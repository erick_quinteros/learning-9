library(data.table)

system("mkdir builds")
setwd("builds")
system("mkdir 9ccccdae-a950-4f07-a99c-0b1727245521")
system("mkdir marc-test-builduid")

learning.properties <- data.table(v=c("#Fri Sep 29 17:40:03 UTC 2017","LE_MT_RFtreeNo=100","LE_MT_addPredictorsFromAccountProduct=channelPreferenceNeg_akt;channelPreferencePos_akt","LE_MT_RFtreeDepth=10","modelType=MTT","productUID=a00A0000000quNsIAI","buildUID=marc-test-builduid","LE_MT_lookBack=61"))

write(learning.properties$v,file="marc-test-builduid/learning.properties")

learning.properties <- data.table(v=c("#Fri Sep 29 17:40:03 UTC 2017","LE_MS_RFtreeNo=100","LE_MS_addPredictorsFromAccountProduct=channelPreferenceNeg_akt;channelPreferencePos_akt","LE_MS_RFtreeDepth=10","modelType=MSO","LE_MS_removeMessageSends=0","LE_MS_removeMessageOpens=0","LE_MS_newMessageStrategy=Conservative","productUID=a00A0000000quNsIAI","LE_MS_removeMessageClicks=0","LE_MS_messageAnalysisTargetType=OPEN","buildUID=9ccccdae-a950-4f07-a99c-0b1727245521","channelUID=SEND_CHANNEL"))

write(learning.properties$v,file="9ccccdae-a950-4f07-a99c-0b1727245521/learning.properties")
