#### this function equal to the following expr
### t <- melt(df,id_vars, value_vars = NULL, var_name = "key", value_name = "value")
### t <- t[t>0] (if filterZero=TRUE)
### t[, (newColName):=paste(var_name,value_name,sep="_)]
sparkMeltCombine <- function(df, id_vars, value_vars = NULL, var_name = "key", value_name = "value", filterZero=TRUE, newColName=NULL, keepVarValueCols=TRUE) {
  # Alias for the output view
  alias <- paste(deparse(substitute(df)), stri_rand_strings(1, 10), sep = "_")
  # Get session and JVM object
  sc <- spark_connection(df)
  # spark <- spark_session(sc)
  jdf <- spark_dataframe(df)
  # apply melt
  jdf <- .sparkMelt(sc, jdf, id_vars, value_vars, var_name, value_name)
  # apply filterZero
  if (filterZero) {
    jdf <- jdf %>%
      invoke("filter", paste(value_name,">0",sep=""))
  }
  # apply change value column to character
  jdf <- .sparkColTypeChange(sc, jdf, value_name, "String", firstCovertToLong=TRUE)
  # apply combine
  jdf <- .sparkCombineCols(sc, jdf, c(var_name, value_name), sep="_", newColName, keepVarValueCols)
  # Explode and register as temp table
  jdf %>%
    invoke("createOrReplaceTempView", alias)
  # return
  return(dplyr::tbl(sc, alias))
}

#### this function equal to the following expr
### t <- melt(df,id_vars, value_vars = NULL, var_name = "key", value_name = "value")
### t <- t[t>0] (if filterZero=TRUE)
### t[, (newColName):=paste(var_name,value_name,sep="_)]
### t[,c(id_vars,newColName)]
sparkMeltCombineDirect<- function(df, id_vars, value_vars=NULL, var_name = "key", value_name = "value", filterZero=TRUE, newColName=NULL) {
  if (is.null(value_vars)) {value_vars <- colnames(df)[!colnames(df) %in% id_vars]}
  if (is.null(newColName)) {newColName <- "col"}
  # Alias for the output view
  alias <- paste(deparse(substitute(df)), stri_rand_strings(1, 10), sep = "_")
  # Get session and JVM object
  sc <- spark_connection(df)
  # spark <- spark_session(sc)
  jdf <- spark_dataframe(df)
  # Combine columns into array<struct<key,value>> and explode
  exploded <- sqlf(sc, "explode")(sqlf(sc, "array")(lapply(value_vars, function(x) {
    key <- sqlf(sc, "lit")(x)
    value <- sqlf(sc, "col")(x) %>% invoke("cast","LONG") %>% invoke("cast","String")
    sqlf(sc, "concat_ws")("_",list(key,value))
  })))
  # expand struct<..., struct<key, value>> into struct<..., key, value>
  exprs <- lapply(
    c(id_vars, "col"),
    sqlf(sc, "col"))
  # Explode and register as temp table
  jdf %>%
    invoke("withColumn", "col", exploded) %>%
    invoke("select", exprs) %>%
    invoke("createOrReplaceTempView", alias)
  new_df <- dplyr::tbl(sc, alias)
  if (filterZero) {new_df <- new_df %>% filter(!rlike(col,"_0$"))}
  # return
  return(new_df)
}